package app;

public class Amount {
    private Integer amount;

    public Amount(String amount) {
        try {
            this.amount = Integer.parseInt(amount);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid amount");
        }
    }

    public Integer getAmount() {
        return amount;
    }
}
