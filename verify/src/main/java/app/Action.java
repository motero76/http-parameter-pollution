package app;

public enum Action {
    WITHDRAW,
    TRANSFER;

    public static Action fromValue(String action) {
        if ("withdraw".equalsIgnoreCase(action)) {
            return WITHDRAW;
        }
        if ("transfer".equalsIgnoreCase(action)) {
            return TRANSFER;
        } else {
            throw new IllegalArgumentException("action not permitted");
        }
    }
}
