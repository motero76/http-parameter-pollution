package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Security unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {
    
    @Autowired
    private MockMvc mockMvc;

    static {
        // Virtualbox: http://10.0.2.15:8082/
        // Osx: http://host.docker.internal:8082/
        // Other systems: http://(IP address of docker host):8082/
        System.setProperty("paymenturl", "http://host.docker.internal:8082//");
    }

    @Test
    public void should_rejectWithdraw() throws Exception {
        Exception exception = Assertions.assertThrows(NestedServletException.class, () -> {
            this.mockMvc.perform(post("/").param("action","transfer").param("amount","100%26action=withdraw"))
                    .andExpect(status().isOk())
                    .andExpect(content().string("Fake Payment Controller: Successfully transfered $100%26action%3Dwithdraw"));
        });
        String expectedMessage = "Invalid amount";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
